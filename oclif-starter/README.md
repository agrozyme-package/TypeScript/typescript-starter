oclif-hello-world
=================

oclif example Hello World CLI

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![CircleCI](https://circleci.com/gh/oclif/hello-world/tree/main.svg?style=shield)](https://circleci.com/gh/oclif/hello-world/tree/main)
[![GitHub license](https://img.shields.io/github/license/oclif/hello-world)](https://github.com/oclif/hello-world/blob/main/LICENSE)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g oclif-starter
$ oclif-starter COMMAND
running command...
$ oclif-starter (--version)
oclif-starter/0.0.0 win32-x64 node-v20.4.0
$ oclif-starter --help [COMMAND]
USAGE
  $ oclif-starter COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`oclif-starter hello PERSON`](#oclif-starter-hello-person)
* [`oclif-starter hello world`](#oclif-starter-hello-world)
* [`oclif-starter help [COMMANDS]`](#oclif-starter-help-commands)
* [`oclif-starter plugins`](#oclif-starter-plugins)
* [`oclif-starter plugins:install PLUGIN...`](#oclif-starter-pluginsinstall-plugin)
* [`oclif-starter plugins:inspect PLUGIN...`](#oclif-starter-pluginsinspect-plugin)
* [`oclif-starter plugins:install PLUGIN...`](#oclif-starter-pluginsinstall-plugin-1)
* [`oclif-starter plugins:link PLUGIN`](#oclif-starter-pluginslink-plugin)
* [`oclif-starter plugins:uninstall PLUGIN...`](#oclif-starter-pluginsuninstall-plugin)
* [`oclif-starter plugins:uninstall PLUGIN...`](#oclif-starter-pluginsuninstall-plugin-1)
* [`oclif-starter plugins:uninstall PLUGIN...`](#oclif-starter-pluginsuninstall-plugin-2)
* [`oclif-starter plugins update`](#oclif-starter-plugins-update)

## `oclif-starter hello PERSON`

Say hello

```
USAGE
  $ oclif-starter hello PERSON -f <value>

ARGUMENTS
  PERSON  Person to say hello to

FLAGS
  -f, --from=<value>  (required) Who is saying hello

DESCRIPTION
  Say hello

EXAMPLES
  $ oex hello friend --from oclif
  hello friend from oclif! (./src/commands/hello/index.ts)
```

_See code: [dist/commands/hello/index.ts](https://github.com/agrozyme/oclif-starter/blob/v0.0.0/dist/commands/hello/index.ts)_

## `oclif-starter hello world`

Say hello world

```
USAGE
  $ oclif-starter hello world

DESCRIPTION
  Say hello world

EXAMPLES
  $ oclif-starter hello world
  hello world! (./src/commands/hello/world.ts)
```

## `oclif-starter help [COMMANDS]`

Display help for oclif-starter.

```
USAGE
  $ oclif-starter help [COMMANDS] [-n]

ARGUMENTS
  COMMANDS  Command to show help for.

FLAGS
  -n, --nested-commands  Include all nested commands in the output.

DESCRIPTION
  Display help for oclif-starter.
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v5.2.10/src/commands/help.ts)_

## `oclif-starter plugins`

List installed plugins.

```
USAGE
  $ oclif-starter plugins [--core]

FLAGS
  --core  Show core plugins.

DESCRIPTION
  List installed plugins.

EXAMPLES
  $ oclif-starter plugins
```

_See code: [@oclif/plugin-plugins](https://github.com/oclif/plugin-plugins/blob/v2.4.7/src/commands/plugins/index.ts)_

## `oclif-starter plugins:install PLUGIN...`

Installs a plugin into the CLI.

```
USAGE
  $ oclif-starter plugins:install PLUGIN...

ARGUMENTS
  PLUGIN  Plugin to install.

FLAGS
  -f, --force    Run yarn install with force flag.
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Installs a plugin into the CLI.
  Can be installed from npm or a git url.

  Installation of a user-installed plugin will override a core plugin.

  e.g. If you have a core plugin that has a 'hello' command, installing a user-installed plugin with a 'hello' command
  will override the core plugin implementation. This is useful if a user needs to update core plugin functionality in
  the CLI without the need to patch and update the whole CLI.


ALIASES
  $ oclif-starter plugins add

EXAMPLES
  $ oclif-starter plugins:install myplugin 

  $ oclif-starter plugins:install https://github.com/someuser/someplugin

  $ oclif-starter plugins:install someuser/someplugin
```

## `oclif-starter plugins:inspect PLUGIN...`

Displays installation properties of a plugin.

```
USAGE
  $ oclif-starter plugins:inspect PLUGIN...

ARGUMENTS
  PLUGIN  [default: .] Plugin to inspect.

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

GLOBAL FLAGS
  --json  Format output as json.

DESCRIPTION
  Displays installation properties of a plugin.

EXAMPLES
  $ oclif-starter plugins:inspect myplugin
```

## `oclif-starter plugins:install PLUGIN...`

Installs a plugin into the CLI.

```
USAGE
  $ oclif-starter plugins:install PLUGIN...

ARGUMENTS
  PLUGIN  Plugin to install.

FLAGS
  -f, --force    Run yarn install with force flag.
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Installs a plugin into the CLI.
  Can be installed from npm or a git url.

  Installation of a user-installed plugin will override a core plugin.

  e.g. If you have a core plugin that has a 'hello' command, installing a user-installed plugin with a 'hello' command
  will override the core plugin implementation. This is useful if a user needs to update core plugin functionality in
  the CLI without the need to patch and update the whole CLI.


ALIASES
  $ oclif-starter plugins add

EXAMPLES
  $ oclif-starter plugins:install myplugin 

  $ oclif-starter plugins:install https://github.com/someuser/someplugin

  $ oclif-starter plugins:install someuser/someplugin
```

## `oclif-starter plugins:link PLUGIN`

Links a plugin into the CLI for development.

```
USAGE
  $ oclif-starter plugins:link PLUGIN

ARGUMENTS
  PATH  [default: .] path to plugin

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Links a plugin into the CLI for development.
  Installation of a linked plugin will override a user-installed or core plugin.

  e.g. If you have a user-installed or core plugin that has a 'hello' command, installing a linked plugin with a 'hello'
  command will override the user-installed or core plugin implementation. This is useful for development work.


EXAMPLES
  $ oclif-starter plugins:link myplugin
```

## `oclif-starter plugins:uninstall PLUGIN...`

Removes a plugin from the CLI.

```
USAGE
  $ oclif-starter plugins:uninstall PLUGIN...

ARGUMENTS
  PLUGIN  plugin to uninstall

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Removes a plugin from the CLI.

ALIASES
  $ oclif-starter plugins unlink
  $ oclif-starter plugins remove
```

## `oclif-starter plugins:uninstall PLUGIN...`

Removes a plugin from the CLI.

```
USAGE
  $ oclif-starter plugins:uninstall PLUGIN...

ARGUMENTS
  PLUGIN  plugin to uninstall

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Removes a plugin from the CLI.

ALIASES
  $ oclif-starter plugins unlink
  $ oclif-starter plugins remove
```

## `oclif-starter plugins:uninstall PLUGIN...`

Removes a plugin from the CLI.

```
USAGE
  $ oclif-starter plugins:uninstall PLUGIN...

ARGUMENTS
  PLUGIN  plugin to uninstall

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Removes a plugin from the CLI.

ALIASES
  $ oclif-starter plugins unlink
  $ oclif-starter plugins remove
```

## `oclif-starter plugins update`

Update installed plugins.

```
USAGE
  $ oclif-starter plugins update [-h] [-v]

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Update installed plugins.
```
<!-- commandsstop -->
